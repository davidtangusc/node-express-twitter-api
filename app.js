require('dotenv').config();
const express = require('express');
const Twitter = require('twitter');
const cors = require('cors');
const app = express();

app.use(cors()); // middleware

app.get('/tweets', function(request, response) {
  if (!request.query.screenname) {
    return response.status(422).json({
      error: 'You must provide a screenname query param'
    });
  }

  var client = new Twitter({
    consumer_key: process.env.CONSUMER_KEY,
    consumer_secret: process.env.CONSUMER_SECRET,
    bearer_token: process.env.BEARER_TOKEN
  });
   
  var params = { screen_name: request.query.screenname };
  client.get('statuses/user_timeline', params, function(error, tweets) {
    if (!error) {
      response.json(tweets);
    }
  });
});

app.listen(8000);